\section{Problem Statement}
Plan recognition is the problem of inferring the course of action (i.e., plan) an actor may take towards achieving a goal from a sequence of observations \cite{kautz1986generalized,schmidt1978plan}. The constructed plan, if followed to completion, is expected to result in states that correspond to goals of the actor, which in turn presupposes that the actor \textit{intends} to achieve those goals. Plan recognition is the closest well known problem to our focus on identifying actions that contribute to causing known,
undesirable goals. Thus, we adapt the plan recognition problem definition from Ramirez and Geffner \shortcite{ramirez2010probabilistic}.

\subsubsection*{Definition 1} 
A planning domain \textit{is a tuple $ D = \langle F, A\rangle$ where F is a set of fluents and A is a set of actions with preconditions and effects that are fluents.}

\subsubsection*{Definition 2}
A planning problem \textit{is a tuple  $\!P = \langle D, I, G \rangle $, where $D$ is a planning domain, $I\subseteq F$ and $G\subseteq F$} \newline

$F$ defines the states that may hold, and $A$ is the set of actions that can cause needed effects for the domain. $I$ is the initial state, $G$ is the goal state. We assume that the planning domain and problems are represented in PDDL. This allows us to use an existing planner, without loss of generality.

\subsubsection*{Definition 3}
Valid (undesirable) plan $\Pi_u$ \textit{is a solution to the planning problem $P$, which is an action sequence $a_1, a_2, ... ,a_k  \;where\; a_i \in A\; and \; i=1,2, \dots ,k\; (k = $ length of plan) that modifies the initial state into a goal state $G$ by the successive execution of actions in} $\Pi_u$.

\subsubsection*{Definition 4}
An action in a plan \textit{is a tuple $a = \langle Pre(a), Add(a), Del(a)\rangle$, where Pre(a), Add(a) and Del(a) are all in F and refer to preconditions, add effects and delete effects of action respectively.}

\subsubsection*{Definition 5}
An undesirable state anticipation problem \textit{is a tuple $T = \langle D, O, I, U, \alpha \rangle$ where $D$ is a planning domain, $O \subseteq A$ is the sequence of observed actions, $I \subseteq F$ is the set of fluents comprising the initial state, $U \subseteq F$ is a set of undesirable states and $\alpha = [0,0,1]$ is a weight for the objective function.}

Observation sequence $O$, with respect to an undesirable plan $\Pi_u$ that achieves $G$ is a sequence of actions altered for extraneous and missing actions. O is said to be having extraneous actions if there is at least one observation $o \in O$, such that add effects of $o$ is never added to the state by any of the actions in $\Pi_u$. Additionally, $O$ is said to be having missing observations, if there is at least one the actions in $\Pi_u$ that has been omitted from $O$. Algorithm for automating the generation of traces will be presented in section 3. The weight $ \alpha $ designates the relative importance of the three factors that define criticality: certainty, timeliness and desirability.

The solution to the undesirable state anticipation problem is a sequence of actions corresponding to the observations (one for each observation) such that each is judged whether it is a critical trigger action given the observations up to that point in the observation sequence. Given an undesirable plan $\Pi_u$ we distinguish two types of actions leading to one or more undesirable states.

\subsubsection*{Definition 6}
A trigger action \textit{is an action $t_i$ , $(t_i$ $\in A)$ $\land$ $(t_i \in O)$, that is found in at least one undesirable plan}.

\subsubsection*{Definition 7}
A critical trigger action \textit{$c_i$ is the trigger action at observation $o_i$ that maximizes a linear combination of the objective function, $V(c_i) = \alpha\upsilon_1(c_i) + \beta\upsilon_2(c_i) + \gamma\upsilon_2(c_i)$ across all possible trigger actions found give observation sequence O.}

$\upsilon_1, \upsilon_2, \textnormal{and } \upsilon_3$ correspond to the certainty, timeliness and desirability objectives.

In our study, we assume that a goal $G$ can be achieved in alternative ways. For simplicity we restrict the number of alternatives to ten. Thus for our purposes, certainty is measured by the percent of alternative undesirable plans in which the action $a$ occurs:

\begin{equation}
Certainty (a)  = \frac{|\textup{undesirable plans in which}\; a\; \textup{occurs}|}{|\textup{undesirable plans found}|}
\end{equation}

Similarly, precise computation of timeliness requires knowing what is yet to be observed and knowing what could be done with the information (e.g., how the action or its effects could be prevented or circumvented). For our purposes, timeliness is measured by the maximum normalized steps remaining in the set of alternative undesirable plans in which the action occurs:
\debug{sachini}{FIX OVERFLOW}
\begin{equation}
Timeliness (a) = \max_{\pi\in\Pi}\left(\frac{\max(\textup{\# steps in} \;\pi\;\textup{from} \;a)}{\textup{\# steps in}\; \pi}\right)
\end{equation}




\fromnotes{need to adjust example to new metrics}
To illustrate, we present a small example. Assume at a certain point in the observations where $U={u_1,u_2,u_3}$, we have generated a set of undesirable plans $\Pi=\{(bgn[u_1]), (bgmq[u_1]), (ch[u_2]), (cjp[u_1]), (cjdfl[u_3]),$ $(cjdfkp[u_1]), (fl[u_3]), (fkp[u_1]), (fkdfl[u_3]), (fkdfkp[u_1])\}$ where a plan is a sequence of actions (letters) followed in brackets by the undesirable state that results and each letter $(b,c,d,f,g,h,j,k,l,m,n,p,q)$ denotes a trigger action (an action that appears in at least one undesirable plan). From this we can compute the Certainty and Timeliness of each trigger action as shown in Table~\ref{tab:example}.
\begin{table}
  \centering
  \begin{tabular}{|l|l|l|l|}
\hline
 & Certainty & Timeliness & Obj \\
\hline
b & 2/10=0.2 & max(3/3,4/4)=1.0 & 0.6 \\
c & 4/10=0.4 & max(2/2,3/3,5/5,6/6)=1.0 & 0.7 \\
d & 4/10=0.4 & max(3/5,4/6,3/5,4/6)=0.6 & 0.5 \\
f & 6/10=0.6 & max(2/5,3/6,2/2,2/3,5/5,6/6)=1.0 & 0.8 \\
g & 2/10=0.2 & max(2/3,3/4)=0.75 & 0.475 \\
h & 1/10=0.1 & max(1/2)=0.5 & 0.35 \\
j & 3/10=0.3 & max(2/3,4/5,5/6)=0.83 & 0.565 \\
k & 4/10=0.4 & max(2/6,2/3,4/5,5/6)=0.83 & 0.615 \\
l & 3/10=0.3 & max(1/5,1/2,1/5)=0.5 & 0.4 \\
m & 1/10=0.1 & max(2/4)=0.5 & 0.35 \\
n & 1/10=0.1 & max(1/3)=0.33 & 0.215 \\
p & 4/10=0.4 & max(1/3,1/6,1/3,1/6)=0.33 & 0.53 \\
q & 1/10=0.1 & max(1/4)=0.25 & 0.175 \\
\hline    
  \end{tabular}
  \caption{Certainty and Timeliness computations for each trigger action in a synthetic example. Obj is the objective function assuming $\alpha=0.5$.}
\label{tab:example}
\end{table}
If the two objectives are equally weighted ($alpha=0.5$) or if certainty is the only objective ($alpha=1$), then the highest ranked action is $f$; so if $f$ is the observed action, it will be flagged as a critical action. If timeliness is the objective ($alpha=0$), then we have a tie between $b$, $c$ and $f$; thus, if one of them is the observed action, it will be flagged as critical.  











